# Recipe APP APi Proxy

NGNIX proxy app for our recipe app api

## Usage

### Environment Variables

* `LISTEN_PORT` - Port to listen on (default:`8000`)
* `APP_HOST` - Hostname of the app to forward requests to (defaul `app`)
* `APP_PORT` - Port of the app to forward request to (default:`9000`) 